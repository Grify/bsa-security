package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.model.*;
import com.binarystudio.academy.springsecurity.security.model.AuthResponse;
import com.binarystudio.academy.springsecurity.security.model.RefreshTokenDTO;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("auth")
public class AuthController {
	private final AuthService authService;

	public AuthController(AuthService authService) {
		this.authService = authService;
	}

	@PostMapping("login")
	public AuthResponse login(@RequestBody AuthorizationRequest authorizationRequest) {
		return authService.performLogin(authorizationRequest);
	}

	@PostMapping("register")
	public AuthResponse register(@RequestBody RegistrationRequest registrationRequest) {
		return authService.performRegister(registrationRequest);
	}

	@PostMapping("changePassword")
	public AuthResponse changePassword(@RequestBody ChangePasswordRequest request) {
		return authService.changePassword(request);
	}

	@PostMapping("forgotPassword")
	public void forgotPassword(@RequestBody ForgotPasswordRequest forgotPasswordRequest) {
		authService.forgotPassword(forgotPasswordRequest);
	}

	@PostMapping("restorePassword")
	public void restorePassword(@RequestBody RestorePasswordRequest restorePasswordRequest) {
		authService.restorePassword(restorePasswordRequest);
	}

	@PostMapping("refreshToken")
	public AuthResponse refreshToken(@RequestBody RefreshTokenDTO refreshTokenDTO) {
		return authService.generateTokens(refreshTokenDTO);
	}

	@GetMapping("me")
	public User whoAmI(@AuthenticationPrincipal User user) {
		return user;
	}
}
