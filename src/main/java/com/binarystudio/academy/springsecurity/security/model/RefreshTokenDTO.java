package com.binarystudio.academy.springsecurity.security.model;

import lombok.Data;

@Data
public class RefreshTokenDTO {
    private String refreshToken;
}
