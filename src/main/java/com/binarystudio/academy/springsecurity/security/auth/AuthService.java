package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.*;
import com.binarystudio.academy.springsecurity.security.jwt.JwtProvider;
import com.binarystudio.academy.springsecurity.security.model.AuthResponse;
import com.binarystudio.academy.springsecurity.security.model.RefreshTokenDTO;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class AuthService {
	private final UserService userService;
	private final JwtProvider jwtProvider;
	private final PasswordEncoder passwordEncoder;

	public AuthService(UserService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.jwtProvider = jwtProvider;
		this.passwordEncoder = passwordEncoder;
	}

	public AuthResponse performLogin(AuthorizationRequest authorizationRequest) {
		var userDetails = userService.loadUserByUsername(authorizationRequest.getUsername());
		if (passwordsDontMatch(authorizationRequest.getPassword(), userDetails.getPassword())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
		}
		return AuthResponse.of(jwtProvider.generateAccessToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
	}

	public AuthResponse performRegister(RegistrationRequest registrationRequest) {
		var userDetails = userService.createUser(registrationRequest);

		return AuthResponse.of(jwtProvider.generateAccessToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
	}

	public AuthResponse generateTokens(RefreshTokenDTO refreshTokenDTO) {
		String refreshToken = refreshTokenDTO.getRefreshToken();
		String login = jwtProvider.getLoginFromToken(refreshToken);
		var user = userService.loadUserByUsername(login);
		return AuthResponse.of(jwtProvider.generateAccessToken(user), jwtProvider.generateRefreshToken(user));
	}

	public AuthResponse changePassword(ChangePasswordRequest request) {
		var userDetails = userService.loadUserByUsername(request.getUsername());
		if (passwordsDontMatch(request.getCurrentPassword(), userDetails.getPassword())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
		}
		var newUser = userService.changePassword(userDetails, request.getNewPassword());
		return AuthResponse.of(jwtProvider.generateAccessToken(newUser), jwtProvider.generateRefreshToken(newUser));
	}

	public void forgotPassword(ForgotPasswordRequest request) {
		var userDetails = userService.loadUserByUsername(request.getUsername());
		String token = jwtProvider.generateAccessToken(userDetails);
		//Send to email
		System.out.println(token);
	}

	public void restorePassword(RestorePasswordRequest request) {
		String username = jwtProvider.getLoginFromToken(request.getToken());
		var user = userService.loadUserByUsername(username);
		userService.changePassword(user, request.getNewPassword());
	}

	private boolean passwordsDontMatch(String rawPw, String encodedPw) {
		return !passwordEncoder.matches(rawPw, encodedPw);
	}

}
