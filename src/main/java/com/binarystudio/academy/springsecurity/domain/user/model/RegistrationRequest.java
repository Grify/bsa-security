package com.binarystudio.academy.springsecurity.domain.user.model;

import lombok.Data;

@Data
public class RegistrationRequest {
    private String username;
    private String email;
    private String password;
}
