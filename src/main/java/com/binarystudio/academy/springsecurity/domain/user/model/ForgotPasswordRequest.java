package com.binarystudio.academy.springsecurity.domain.user.model;

import lombok.Data;

@Data
public class ForgotPasswordRequest {
    private String username;
    private String email;
}
