package com.binarystudio.academy.springsecurity.domain.user.model;

import lombok.Data;

@Data
public class RestorePasswordRequest {
    private String token;
    private String newPassword;
}
