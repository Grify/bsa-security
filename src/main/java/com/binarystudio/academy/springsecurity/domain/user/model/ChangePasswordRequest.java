package com.binarystudio.academy.springsecurity.domain.user.model;

import lombok.Data;

@Data
public class ChangePasswordRequest {
    private String username;
    private String currentPassword;
    private String newPassword;
}
