package com.binarystudio.academy.springsecurity.domain.user;

import com.binarystudio.academy.springsecurity.domain.user.model.ChangePasswordRequest;
import com.binarystudio.academy.springsecurity.domain.user.model.RegistrationRequest;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.List;

@Service
public class UserService implements UserDetailsService {
	private final UserRepository userRepository;

	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Not found"));
	}

	public User createUser(RegistrationRequest request) {
		return userRepository.createUser(request).orElseThrow(() ->
				new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Name or Email has been taken"));
	}

	public User changePassword(User user, String newPassword) {
		return userRepository.changePassword(user, newPassword).orElseThrow(() ->
				new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is not found"));
	}

	public List<User> getAll() {
		return userRepository.findUsers();
	}
}
